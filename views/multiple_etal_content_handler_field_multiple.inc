<?php

/**
 * @file
 *
 * Add group display options for multiple field.
 */

class multiple_etal_content_handler_field_multiple extends content_handler_field_multiple {

  function options(&$options) {
    parent::options($options);
    $options['multiple'] = array(
      //'group' => TRUE,
      'multiple_etal_limit' => '',
      'multiple_etal_suffix' => '',
    );
  }

  /**
   * Provide 'group multiple values' option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $field = $this->content_field;
    $options = $this->options;

    // Make the string translatable by keeping it as a whole rather than
    // translating prefix and suffix separately.
    list($prefix, $suffix) = explode('@count', t('if more than @count value(s)'));
    $form['multiple']['multiple_etal_limit'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#field_prefix' => $prefix,
      '#field_suffix' => $suffix,
      '#default_value' => $options['multiple']['multiple_etal_limit'],
      '#prefix' => '<div class="container-inline">',
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-options-multiple-group' => array(TRUE)),
    );
    list($prefix, $suffix) = explode('@suffix', t('adding @suffix as suffix'));
    $form['multiple']['multiple_etal_suffix'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#field_prefix' => $prefix,
      '#field_suffix' => $suffix,
      '#default_value' => $options['multiple']['multiple_etal_suffix'],
      '#suffix' => '</div>',
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-options-multiple-group' => array(TRUE)),
      '#description' => t('(leave blank for no suffix)'),
    );
  }

  function render($values) {
    // If this is not a grouped field, use content_handler_field::render().
    if (!$this->defer_query) {
      // This will make parent class trigger it's parent class.
      return parent::render($values);
    }

    // Save original options for reseting reference later.
    $this->options_orig = $this->options;

    $vid = $values->{$this->field_alias};
    if (isset($this->field_values[$vid])) {
      $options = $this->options;
      if (!empty($options['multiple']['multiple_etal_limit'])) {
        if ($options['multiple']['multiple_etal_limit'] < count($this->field_values[$vid])) {
          // Cut a slice of the array according to the paramenters.
          $this->field_values[$vid] = array_slice($this->field_values[$vid], (int) $options['multiple']['multiple_from'], (int) $options['multiple']['multiple_number'], TRUE);
          $etal = check_plain($options['multiple']['multiple_etal_suffix']);
        }
        else {
          // Reset options as they now depend on the limit being reached.
          $this->options['multiple']['multiple_from'] = $this->options['multiple']['multiple_number'] = '';
        }
      }
    }

    // Pass rendering to parent method.
    $output = parent::render($values) . ' ' . ((isset($etal) && !empty($etal)) ? $etal : '');

    // Reset options so changes don't get carried to the next iteration.
    $this->options = $this->options_orig;

    return $output;
  }

}
