<?php

/**
 * @file
 *
 * Provide views metadata.
 */

/**
 * Implementation of hook_views_handlers().
 */
function multiple_etal_views_handlers() {
  return array(
    'handlers' => array(
      'multiple_etal_content_handler_field_multiple' => array(
        'parent' => 'content_handler_field_multiple',
        'path' => drupal_get_path('module', 'multiple_etal') . '/views',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function multiple_etal_views_data_alter(&$data) {
  foreach($data as &$table) {
    foreach($table as &$field) {
      if ($field['field']['handler'] == 'content_handler_field_multiple') {
        $field['field']['handler'] = 'multiple_etal_content_handler_field_multiple';
      }
    }
  }
}
